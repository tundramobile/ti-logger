This directory contains a snapshot of the SQLite trunk source code
as of 2016-02-04 10:28 UTC, concatenated into an "amalgamation".

See https://www.sqlite.org/draft/howtocompile.html for instructions
on how to compile these sources.

See https://www.sqlite.org/amalgamation.html for additional
information about the amalgamated source code format.
