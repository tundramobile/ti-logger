// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.
#ifndef TILOGMAIN_H
#define TILOGMAIN_H

#include <mutex>
#include <vector>
#include <memory>
#include <atomic>
#include <set>
#include <string>

#include <ti-logger/core/Singleton.h>

#define TI_LOG_ENABLED 1

namespace ti
{
    class Listener;
    class Message;
    
    enum class LogLevel
    {
        None = -1,
        Error = 0,
        Warning = 1,
        Info = 2
    };
    
    class Log : public Singleton<Log>
    {
    public:
        static void init(const std::string& dbFolderPath, const std::string& apiKey, const std::string& instanceId);
        friend class Singleton;
        
        static void info(int aCode, const std::string& aDomain, const std::set<int> aTags, const std::string& aText);
        static void info(const std::string& aDomain, const std::set<int> aTags, const std::string& aText);
        static void info(const std::string& aDomain, const std::string& aText);
        
        static void warning(int aCode, const std::string& aDomain, const std::set<int> aTags, const std::string& aText);
        static void warning(const std::string& aDomain, const std::set<int> aTags, const std::string& aText);
        static void warning(const std::string& aDomain, const std::string& aText);
        
        static void error(int aCode, const std::string& aDomain, const std::set<int> aTags, const std::string& aText);
        static void error(int aCode, const std::string& aDomain, const std::string& aText);
        
        void enable(bool value){ mEnabled = value; }
        
		void addListener(Listener*);
        void removeListener(Listener*);
		void removeAllListeners();
        
        std::string getApiKey();
        std::string getInstanceId();
        
    private:
        Log();
        void log(const ti::Message&);
        
		void notify(const Message &aMessage, Listener*);
        
        void setApiKey(std::string apiKey);
        void setInstanceId(std::string instanceId);
        
        std::string mApiKeyString;
        std::string mInstanceIdString;
        
        std::mutex mLock;
        
		std::vector<Listener*> mListeners;
        
        std::atomic_bool mEnabled;
    };
}


#endif
