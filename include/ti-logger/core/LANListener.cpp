// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.
#include <stdio.h> //printf

// socket
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
//
#include <unistd.h>
#include "DispatcherThread.h"
#include "LANListener.h"
#include "Message.h"
#include "TIAppInfo.h"
#include "Utils.h"

using namespace ti;

LANListener::LANListener() : mPort(0), mHost("localhost"), mSocket(-1), mConfigured(false)
{
    auto ptr = new DispatcherThread([this](std::vector<DispatcherThread::tMessageRef> messages){
        this->dispatch(messages);
    }, 2);
    
    mDispatcher = std::unique_ptr<DispatcherThread>(ptr);
}

void LANListener::configure(const std::string& aHost, int aPort)
{
	mHost = aHost;
	mPort = aPort;
    LANListener::connect(aHost, aPort);
    
    mConfigured = true;
    mDispatcher->run();
}

void LANListener::setMsgPerTransaction(int aVal)
{
    mDispatcher->setTransactionThreshold(aVal);
}

void LANListener::acceptMessage (const Message& aMessage)
{
    if (mConfigured.load())
    {
        mDispatcher->push(aMessage);
    }
}


void LANListener::dispatch(const std::vector<DispatcherThread::tMessageRef>& aMsgs)
{
    mRetryCount = 0;
    if (mSocket <=0)
    {
        if (!LANListener::connect(mHost, mPort))
        {
            return;
        }
    }
    std::vector<json11::Json> root;;
    
    for(auto it: aMsgs)
    {
        it->PushSerialized(root);
    }
    json11::Json toJson = root;
    auto dump = toJson.dump();
    bool flag = sendData(dump.c_str());
    
    if (!flag && mRetryCount < kTries)
    {
        ++mRetryCount;
        LANListener::connect(mHost, mPort);
        sendData(dump);
    }
}

bool LANListener::connect(std::string address , int port)
{
	if(mSocket <= 0)
	{
		mSocket = socket(AF_INET , SOCK_STREAM , 0);
		if (mSocket < 0)
		{
			perror("Could not create socket");
		}
		int n = 1;
		setsockopt(mSocket, SOL_SOCKET, SO_NOSIGPIPE, &n, sizeof(n));
	}
	
	if(inet_addr(address.c_str()) == -1)
	{
		struct hostent *he;
		struct in_addr **addr_list;
		if ( (he = gethostbyname( address.c_str() ) ) == NULL)
		{
			herror("gethostbyname");
			return false;
		}
		
		//Cast the h_addr_list to in_addr , since h_addr_list also has the ip address in long format only
		addr_list = (struct in_addr **) he->h_addr_list;
		if (addr_list)
		{
			mServer.sin_addr = *addr_list[0];
		}
	}
	else //plain ip address
	{
		mServer.sin_addr.s_addr = inet_addr( address.c_str() );
	}
	
	mServer.sin_family = AF_INET;
	mServer.sin_port = htons( port );
	if (::connect(mSocket , (struct sockaddr *)&mServer , sizeof(mServer)) < 0)
	{
		return 1;
	}
	return true;
}

bool LANListener::sendData(std::string data)
{
	if (mSocket <=0)
	{
		return false;
	}
	
	if( send(mSocket , data.c_str() , data.size() , 0) < 0)
	{
		closeConnection();
		return false;
	}
	
	return true;
}

void LANListener::closeConnection()
{
	if (mSocket > 0)
	{
		close(mSocket);
	}
	mSocket = -1;
}

LANListener::~LANListener()
{
	closeConnection();
}
