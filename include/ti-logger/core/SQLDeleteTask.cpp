//
//  SQLDeleteTask.cpp
//  TundraIntegration
//
//  Created by Stanislav Bessonov on 3/18/16.
//  Copyright © 2016 Tundramobile. All rights reserved.
//

#include <ti-logger/core/SQLDeleteTask.h>
#include <sqlite3.h>
#include <ti-logger/core/Utils.h>
#include <ti-logger/core/DBManager.h>

#define secondsInDay 86400
#define mcSecInSec 1000000

using namespace ti;

SQLDeleteTask::SQLDeleteTask(std::function<void(SQLResult)> aCb):
cbOnResult(aCb)
{
    
}

void SQLDeleteTask::request(sqlite3* aDB)
{
    SQLResult result;
    
    int code = SQLITE_OK;

    auto& table = DBManager::kContentTableName;

    auto time = std::chrono::system_clock::now();
    
    auto timeInMsec = std::chrono::duration_cast<std::chrono::microseconds>(time.time_since_epoch());
    
    auto mcSecInDay = ((long)secondsInDay * (long)mcSecInSec);
   
    std::string aDeleteRequest = "DELETE FROM " + table + " WHERE TIMESTAMP <= " + std::to_string((timeInMsec.count() - mcSecInDay)) + ";";
    
    code = processRequest(aDB, aDeleteRequest);
    
    result.Code = code;
    onCompletion(std::move(result));
}

int SQLDeleteTask::processRequest(sqlite3* aDB, const std::string& aRequest)
{
    char *zErrMsg = nullptr;
    const int rc = ::sqlite3_exec(aDB, aRequest.c_str(), nullptr, nullptr, &zErrMsg);
    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    
    return rc;
}