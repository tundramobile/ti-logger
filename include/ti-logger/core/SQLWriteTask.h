// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <functional>
#include <vector>
#include <ti-logger/core/Message.h>
#include <ti-logger/core/ISQLTask.h>
#include <ti-logger/core/SQLResult.h>

class sqlite3;
namespace ti
{
    class SQLWriteTask : public ISQLTask
    {
    public:
        SQLWriteTask(const std::vector<Message>& aMessages, std::function<void(SQLResult)>);
        
        void request(sqlite3* aDB) override;
        
    private:
        void onCompletion(const SQLResult& aResult) const override
        {
            cbOnResult(aResult);
        }
        
        int processRequest(sqlite3* aDB, const std::string& aRequest);
        
        std::vector<Message> mMessages;
        std::function<void(const SQLResult&)> cbOnResult;
    };
}