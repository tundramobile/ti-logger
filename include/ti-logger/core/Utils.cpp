// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <string>
#include <string>
#include <cstdio>
#include <vector>
#include <sstream>

#include <ti-logger/core/Message.h>
#include <ti-logger/core/Utils.h>

using namespace ti;


using namespace std; //Don't if you're in a header-file

std::string Utils::tagsToString (const std::set<int> aTags)
{
    std::string aResult;
    aResult.reserve(16);
    
    for (auto i : aTags)
    {
        aResult = aResult + std::to_string(i) + ",";
    }
    return aResult;
}

static std::vector<std::string> &split(const std::string & aInput, const char aDelimiter, std::vector<std::string> & aElements)
{
    std::stringstream aStringsStream(aInput);
    std::string aItem;
    while (std::getline(aStringsStream, aItem, aDelimiter))
    {
        aElements.push_back(aItem);
    }
    return aElements;
}

static std::vector<std::string> split(const std::string &aInput, char aDelimiter)
{
    std::vector<std::string> aElements;
    split(aInput, aDelimiter, aElements);
    return std::move(aElements);
}

static std::set<int> stringToTags(std::string aInputString)
{
    std::set<int> aResult;
    if (aInputString.length() < 2)
    {
        return aResult;
    }
    
    
    aInputString.pop_back();
    aInputString = aInputString.substr(1,aInputString.size());
    std::vector<std::string> aElements = split(aInputString, ',');
    std::istringstream aConverter;
    int i;
    for (std::vector<std::string>::iterator it = aElements.begin() ; it != aElements.end(); ++it)
    {
        aConverter.str(*it);
        aConverter >> i;
        aResult.insert(i);
        aConverter.clear();
    }
    
    return std::move(aResult);
}

std::string Utils::toSQLRequest(const std::string& aTable, const Message& it)
{
    return std::string("INSERT INTO " + aTable + " (LEVEL,CODE,DOMAIN,TAGS,DESCRIPTION,TIMESTAMP) "  \
            "VALUES (" + std::to_string(static_cast<int>(it.Level)) + ", " \
            + std::to_string(it.Code) + ", '" \
            + it.Domain + "', '" \
            + ti::Utils::tagsToString(it.Tags) + "', '" \
            + it.Description + "', " \
            + std::to_string(it.Timestamp) + ");");
}

Message Utils::parseSQLResponseToMessage(std::string aLevel, std::string aCode, std::string aDomain, std::string aTags, std::string aDescription, std::string aTimeStamp)
{
    LogLevel aLvl = static_cast<LogLevel>(std::stoi(aLevel));
    int aCd = std::stoi(aCode);
    std::set<int> aTg = stringToTags(aTags);
    long long aTS = stoll(aTimeStamp); //Log::LogLevel aLevel, int aCode, std::string aDomain, std::set<int> aTags, std::string aDescription
    Message aResult = Message(aLvl, aCd, aDomain, aTg, aTS, aDescription);
    
    return std::move(aResult);
}


std::string Utils::strFormat(const char* aFmt, ...)
{
	va_list vl;
	
	va_start(vl, aFmt);
	int size = vsnprintf(0, 0, aFmt, vl) + sizeof('\0');
	va_end(vl);
	
	char buffer[size];
	
	va_start(vl, aFmt);
	size = vsnprintf(buffer, size, aFmt, vl);
	va_end(vl);
	
	return std::string(buffer, size);
}

