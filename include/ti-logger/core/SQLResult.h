// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.
#pragma once

#include <vector>
#include <string>

#include <ti-logger/core/Message.h>

namespace ti
{
	typedef std::vector<std::string> SQLDataRow;
	typedef std::vector<SQLDataRow> SQLData;
    class SQLResult
    {
    public:
     	SQLData data;
		std::vector<Message> getMessages() const;
		int Code;
        bool success() { return !Code;}
    };
}
