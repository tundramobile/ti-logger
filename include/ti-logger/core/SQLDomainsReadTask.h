// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.
#pragma once

#include <functional>

#include <ti-logger/core/Filter.h>
#include <ti-logger/core/ISQLTask.h>

namespace ti
{
	class SQLDomainsReadTask : public ISQLTask
	{
	public:
		SQLDomainsReadTask(std::function<void(SQLResult)> aCB):
		cbOnResult(aCB)
		{}
		void request(sqlite3* aDB) override;
		
	private:
		void onCompletion(const SQLResult& aResult) const override
		{
			cbOnResult(aResult);
		}
		std::function<void(const SQLResult&)> cbOnResult;
	};
}