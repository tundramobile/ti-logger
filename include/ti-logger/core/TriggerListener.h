// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <ti-logger/core/Listener.h>
#include <vector>

namespace ti
{
    class Trigger;

    class TriggerListener : public Listener
    {
    private:
        std::vector<Trigger> mTriggers;
        
    public:
        virtual bool canAccept(const Message& aMessage) = 0;
        virtual void accept (const Message& aMessage) = 0;
    private:
        
    };
}
