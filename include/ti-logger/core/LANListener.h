// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <string>

#include <ti-logger/core/Singleton.h>
#include <ti-logger/core/Listener.h>
#include <mutex>

#include <netdb.h> //hostent

namespace ti
{
    class DispatcherThread;
    class LANListener : public Listener, public Singleton<LANListener>
    {
    public:
        friend class Singleton;
		using Singleton<LANListener>::instance;
		
        void acceptMessage (const Message& aMessage) override;
        void setMsgPerTransaction(int aVal);

		void configure(const std::string& aHost, int aPort);
		
		virtual ~LANListener();
	
    private:
        LANListener();
        
        std::atomic_bool mConfigured;
        
        void dispatch(const std::vector<std::shared_ptr<Message>>& aMsgs);
        
        std::unique_ptr<DispatcherThread> mDispatcher;
        
        const int   kTries = 3;
        
        int         mRetryCount;
        
        int         mPort;
        int         mSocket;
		std::string mHost;
        std::string mAddress;
        struct sockaddr_in mServer;
		
		bool        connect(std::string, int);
        bool        sendData(std::string data);
        void        closeConnection();
		std::string receive(int);
    };
}













