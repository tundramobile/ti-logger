// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <chrono>

#include <ti-logger/core/Message.h>

using namespace ti;


Message::Message(LogLevel aLogLevel,
                 int aCode,
                 const std::string& aDomain,
                 const std::set<int>& aTags,
                 const std::string& aDescription):
Level(aLogLevel),
Code(aCode),
Domain(aDomain),
Tags(aTags),
Timestamp(timestamp()),
Description(aDescription)
{ 
}

Message::~Message()
{
    
}


Message::Message(LogLevel aLogLevel,
                 int aCode,
                 const std::string& aDomain,
                 const std::set<int>& aTags,
                 long long aTs,
                 const std::string& aDescription):
Level(aLogLevel),
Code(aCode),
Domain(aDomain),
Tags(aTags),
Timestamp(aTs),
Description(aDescription)
{
}

Message::Message(LogLevel aLogLevel,
                 const std::string& aDomain,
                 const std::set<int>& aTags,
                 const std::string& aDescription):
Level(aLogLevel),
Code(0),
Domain(aDomain),
Tags(aTags),
Timestamp(timestamp()),
Description(aDescription)
{
}


Message::Message(LogLevel aLogLevel,
                 const std::string& aDomain,
                 const std::string& aDescription):
Level(aLogLevel),
Code(0),
Domain(aDomain),
Tags(),
Timestamp(timestamp()),
Description(aDescription)
{
}

Message::Message(const Message& aOther):
Level(aOther.Level),
Code(aOther.Code),
Domain(aOther.Domain),
Tags(aOther.Tags),
Timestamp(aOther.Timestamp),
Description(aOther.Description)
{
    
}


Message& Message::operator=(const Message& other)
{
    Level = other.Level;
    Code = other.Code;
    Domain = other.Domain;
    Tags = other.Tags;
    Timestamp = other.Timestamp;
    Description = other.Description;
    
    return *this;
}

long long Message::timestamp()
{
    typedef std::chrono::microseconds tMsec;
    typedef std::chrono::system_clock tSclock;
    
    auto span = std::chrono::duration_cast<tMsec>(tSclock::now().time_since_epoch());
    return span.count(); 
}

void Message::Deserialize(const json11::Json& aNode)
{
    auto fields = aNode.object_items();
    
    Level = (LogLevel)fields["level"].int_value();
    Code = fields["code"].int_value();
    Domain = fields["domain"].string_value();
    
    auto tags = std::set<json11::Json>(fields["tags"].array_items().begin(),
                                       fields["tags"].array_items().end());
    
    for (auto tg: tags)
    {
        Tags.insert(tg.int_value());
    }
    
    Timestamp   = fields["timestamp"].number_value();
    Description = fields["description"].string_value();
}

void Message::Serialize(json11::Json& aNode)
{
    assert(aNode.is_object());
    auto fields = aNode.object_items();
    
    fields["level"] = json11::Json((int)Level);
    fields["code"] = json11::Json((int)Code);
    fields["domain"] = json11::Json(Domain);
    fields["tags"] = json11::Json(Tags);
    fields["timestamp"] = json11::Json((double)Timestamp);
    fields["description"] = json11::Json(Description);
    
    aNode = fields;
}














