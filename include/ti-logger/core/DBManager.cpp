// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <stdio.h>

#include <ti-logger/core/DBManager.h>

using namespace ti;

const std::string DBManager::kContentTableName = "CONTENT";
const char* kDatabaseName = "local.db";

static auto kRecordTimeout = std::chrono::seconds(60*60);

static auto kDeleteTime = std::chrono::seconds(60);

DBManager::DBManager():
mDie(false),
mInitialized(false)
{
    
}

DBManager::~DBManager()
{
    mDie.exchange(true);
    mWorker.join();
    sqlite3_exec(mDatabase, "END TRANSACTION", nullptr, nullptr, nullptr);
    sqlite3_close(mDatabase);
}

void DBManager::init(const std::string& aDataBaseFolderPath)
{
    if (mInitialized.load())
        return;
    
    std::lock_guard<std::mutex> scopedLock(mTableLock);
	const std::string filePath(aDataBaseFolderPath.empty() ? kDatabaseName: aDataBaseFolderPath + "/" + kDatabaseName);
	
    sqlite3* ptr;
    int res = sqlite3_open(filePath.c_str(), &ptr);
	if (res)
	{
		fprintf(stderr, "Can't open database: %s\n%s\n", sqlite3_errmsg(mDatabase), filePath.c_str());
	}
	
    mDatabase = ptr;
    sqlite3_exec(mDatabase, "PRAGMA synchronous = OFF", nullptr, nullptr, nullptr);
    
    if (!hasTalble())
        createTable();
    
    mInitialized.exchange(true);
    this->mWorker = std::thread(&DBManager::loop, this);
}

void DBManager::drop()
{
    std::lock_guard<std::mutex> scopedLock(mTableLock);
	sqlite3_exec(mDatabase, ("DROP TABLE " + kContentTableName).c_str(), nullptr, nullptr, nullptr);
	createTable();
}

void DBManager::createTable()
{
    std::string aRequest = "CREATE TABLE IF NOT EXISTS " + kContentTableName + " ("  \
    "ID INTEGER PRIMARY KEY    AUTOINCREMENT," \
    "LEVEL				INT    NOT NULL," \
    "CODE				INT     NOT NULL," \
    "DOMAIN				TEXT," \
    "TAGS				TEXT," \
    "DESCRIPTION		TEXT," \
    "TIMESTAMP			UNSIGNED BIG INT);";
    
    sqlite3_stmt* aStatement = NULL;
    
    const int state = sqlite3_prepare_v2(mDatabase, aRequest.c_str(), -1, &aStatement, 0);
    if (state == SQLITE_OK)
    {
        sqlite3_step(aStatement);
        sqlite3_finalize(aStatement);
    }
    fprintf(stderr, "Table has been created with result: %s\n", sqlite3_errmsg(mDatabase));
}

bool DBManager::hasTalble() const
{
    bool flag = false;
    std::string rq = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + kContentTableName + "';";
    sqlite3_stmt* aStatement = NULL;
    
    const int state = sqlite3_prepare_v2(mDatabase, rq.c_str(), -1, &aStatement, nullptr);
    if (state == SQLITE_OK)
    {
        const int code = sqlite3_step(aStatement);
        
        flag = (code == SQLITE_ROW);
        sqlite3_finalize(aStatement);
    }
    
    return flag;
}

void DBManager::pushTask(std::shared_ptr<ISQLTask> task)
{
    mTasks.pushShared(task);
}

bool DBManager::isTimeToClear()
{
    return (mLastClear - std::chrono::system_clock::now()) > kDeleteTime;
}

void DBManager::deleteOldRecords()
{
    auto time = std::chrono::system_clock::now() - kRecordTimeout;
    auto msec = std::chrono::duration_cast<std::chrono::milliseconds>(time.time_since_epoch());
    
    sqlite3_stmt* aStatement = NULL;
    std::string aDeleteRequest = "DELETE FROM " + kContentTableName + " WHERE TIMESTAMP <= " + std::to_string(msec.count()) + ";";
    
    int aState = sqlite3_prepare_v2(mDatabase, aDeleteRequest.c_str(), -1, &aStatement, 0);
    if (aState == SQLITE_OK)
    {
        sqlite3_step(aStatement);
        sqlite3_finalize(aStatement);
    }
    else
    {
        fprintf(stderr, "Can't delete all old data from content table: %s\n", sqlite3_errmsg(mDatabase));
    }
    
    //    aDeleteRequest = "DELETE FROM " + kContentTableName + " WHERE TIMESTAMP <= " \
    //    + std::to_string(aTimeNow - kPeriodOfTimeToForgettingInfo) + " AND LEVEL = " \
    //    + std::to_string(static_cast<int>(LogLevel::Info)) + ";";
    //
    //    aState = sqlite3_prepare_v2(mDatabase, aDeleteRequest.c_str(), -1, &aStatement, 0);
    //    if (aState == SQLITE_OK)
    //    {
    //        sqlite3_step(aStatement);
    //        sqlite3_finalize(aStatement);
    //    } else
    //    {
    //        fprintf(stderr, "Can't delete old info data from content table: %s\n", sqlite3_errmsg(mDatabase));
    //    }
}

void DBManager::loop()
{
    assert(mInitialized.load());
    while (!mDie.load())
    {
        std::shared_ptr<ISQLTask> task = mTasks.waitAndDequeue();
        
        std::lock_guard<std::mutex> scopedLock(mTableLock); 
        task->request(mDatabase);
        
        if (isTimeToClear())
        {
            mLastClear = std::chrono::system_clock::now();
            this->deleteOldRecords();
        }
    }
}
















