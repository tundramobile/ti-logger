// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.
#pragma once

#include <vector>
#include <string>

#include <ti-logger/core/Listener.h>
#include <ti-logger/core/Singleton.h>

namespace ti
{
    class HttpClient;
    class DispatcherThread;
    class Message;
    
	class WEBListener : public Singleton<WEBListener>, public Listener
	{
    public:
        typedef std::shared_ptr<Message> tMessageRef;
		void init(const std::string& aHost, int aPort);
		WEBListener();
		~WEBListener();
		
		bool canAccept(const Message& aMessage) const;
		void acceptMessage(const Message& aMessage);
        
        void setMsgPerTransaction(int aVal);
        
	private:
		std::string mHost;
		int         mPort;
		
        std::unique_ptr<DispatcherThread> mDispatcher;
        
		void dispatch(const std::vector<std::shared_ptr<Message>>& aMsgs);
		
		HttpClient* mHttpClient;
		std::vector<Message> mMessageVector;
	};
} 


