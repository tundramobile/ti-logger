// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.
#pragma once

#include <assert.h>
#include <memory>
#include <string>
#include <vector>
#include <json11/json11.hpp>

namespace jcpp
{
	class IJSerializable
	{
	public: 
		virtual void Deserialize(const json11::Json& aNode) = 0;
		virtual void Serialize(json11::Json& aNode) = 0;
        
        virtual const std::string& getTag() = 0;

        void PushSerialized(std::vector<json11::Json>& array)
		{
            array.push_back(Serialize());
		}

		json11::Json Serialize()
		{
            json11::Json node = json11::Json::object();
			Serialize(node);
			return node;
		}
	};
}