// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#if defined(__APPLE__)

    #include "TargetConditionals.h"

    #if TARGET_OS_IPHONE

        #define TI_PLATFORM_IOS
        #define TI_PLATFORM_IOS_SIMULATOR 0

    #elif TARGET_OS_IPHONE_SIMULATOR

        #define TI_PLATFORM_IOS
        #define TI_PLATFORM_IOS_SIMULATOR 1

    #elif TARGET_OS_MAC

        #define TI_PLATFORM_MAC

    #endif

#elif defined(ANDROID)

        #define TI_PLATFORM_ANDROID

#elif defined(WP8) && defined(_WP8)

        #define TI_PLATFORM_WP8

#elif defined(_WIN32)

        #define TI_PLATFORM_WIN32

#elif defined(LINUX)

        #define TI_PLATFORM_LINUX

#elif defined(TIZEN)

        #define TI_PLATFORM_TIZEN

#else

        #define TI_PLATFORM_UNSUPPORTED

#endif

#if defined(TI_PLATFORM_IOS) || defined(TI_PLATFORM_MAC) || defined(TI_PLATFORM_ANDROID) || defined(TI_PLATFORM_LINUX) || defined(TI_PLATFORM_TIZEN)

    #define TI_SUPPORTS_POSIX 1
    #define TI_SUPPORTS_WIN 0

#elif defined(TI_PLATFORM_WIN32) || defined(TI_PLATFORM_WP8)

    #define TI_SUPPORTS_POSIX 0
    #define TI_SUPPORTS_WIN 1

#endif

#if defined(__LP64__)

    #define TI_ARCH_64BIT 1
    #define TI_ARCH_32BIT 0

#else

    #define TI_ARCH_64BIT 0
    #define TI_ARCH_32BIT 1

#endif
