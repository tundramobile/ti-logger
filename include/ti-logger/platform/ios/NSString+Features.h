//
//  NSString+Features.h
//  TundraIntegration
//
//  Created by Stanislav Bessonov on 3/15/16.
//  Copyright © 2016 Tundramobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Features)

- (NSString*) MD5String;
- (NSString*) generateRandomString:(int)num;


@end
