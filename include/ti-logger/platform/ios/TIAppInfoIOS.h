//
//  TIAppInfoIOS.hpp
//  TundraIntegration
//
//  Created by Anton Komir on 28.01.16.
//  Copyright © 2016 Stanislav Bessonov. All rights reserved.
//

#pragma once

#include <stdio.h>
#include <ti-logger/platform/TIAppInfo.h>

namespace ti {
	class AppInfoIOS : public AppInfo
	{
	public:
		static const char* getDeviceID ();
		static const char* getAppID ();
	};
}