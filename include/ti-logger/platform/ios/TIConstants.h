// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#ifndef Constants_h
#define Constants_h

typedef enum
{
    TIPrintListener = 0,
    TILocalListener = 1,
    TIWEBListener = 2,
    TILANListener = 3
} TIListenerID;

#endif /* Constants_h */
