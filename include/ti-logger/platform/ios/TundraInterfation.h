//
//  TundraInterfation.h
//  TundraInterfation
//
//  Created by Vlad Joss on 2/22/16.
//  Copyright © 2016 Tundramobile. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TundraInterfation.
FOUNDATION_EXPORT double TundraInterfationVersionNumber;

//! Project version string for TundraInterfation.
FOUNDATION_EXPORT const unsigned char TundraInterfationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TundraInterfation/PublicHeader.h>

#include <ti-logger/platform/ios/TILogInterface.h>
