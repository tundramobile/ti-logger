// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#import <Foundation/Foundation.h>
#import <ti-logger/platform/ios/TIOBJMessage.h>

@interface TILog : NSObject

+(NSString*)apiKey;
+(NSString*)instanceId;

+(void)logInitIOS:(NSString*)apiKey instanceId:(NSString*)instanceId;
+(void)info: (NSString* const )aDomain tags:(const NSArray*) aTags text:(NSString* const)aText;
+(void)warning: (NSString* const )aDomain tags:(const NSArray*) aTags text:(NSString* const)aText;
+(void)error:(int) aCode domain:(NSString* const )aDomain tags:(const NSArray*) aTags text:(NSString* const)aText;
+(void)dropContentTable;

+(void)configureWEBListener:(NSString* const )aHost port:(NSInteger)aPort;
+(void)configureLANListener:(NSString* const )aHost port:(NSInteger)aPort;

+(void)addListener:(int)aID;
+(void)delListener:(int)aID;

+(TIOBJMessage*) toObjCMessage: (const void*)aCPPMessage;
+(void)getDomains:(void(^)(NSArray* messages)) onComplete;
+(void)getMessages: (enum TIOBJLogLevel)level Domain:(NSString*)domain tags:(const NSArray*) aTags limit:(NSInteger)limit onComplete:(void(^)(NSArray* messages)) onComplete;

@end
