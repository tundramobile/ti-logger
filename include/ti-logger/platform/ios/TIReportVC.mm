//
//  SendReportViewController.m
//  TI_test
//
//  Created by Stanislav Bessonov on 3/12/16.
//  Copyright © 2016 Tundramobile. All rights reserved.
//

#import "TIReportVC.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <Parse/Parse.h>

#import <ti-logger/platform/ios/TILogVC.h>
#import <ti-logger/platform/ios/TILog.h>
#import <ti-logger/platform/ios/TIOBJMessage.h>

#include <string>
#include <ti-logger/core/Filter.h>
#include <ti-logger/core/SQLReadTask.h>
#include <ti-logger/core/DBManager.h>
#include <ti-logger/core/Utils.h>
#include <json11/json11.hpp>
#include <ti-logger/core/JHelper.h>

#define kDefaultTime @"30"

@interface TIReportVC() <UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate, MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) UITextView *reportTextView;
@property (strong, nonatomic) UIPickerView *timePickerView;
@property (nonatomic, strong) NSArray* timesArray;
@property (nonatomic, readwrite) NSInteger timeValue;

@property (nonatomic, readwrite, strong) NSArray* messages;
@property (nonatomic, assign, readwrite) std::vector<ti::Message> cppMessages;

@end

@implementation TIReportVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setup];
}

- (void)setup
{
    self.view.backgroundColor = [UIColor whiteColor];
    self.reportTextView =  [[UITextView alloc] initWithFrame:CGRectMake(self.view.frame.size.width * 0.1,
                                                                         self.view.frame.size.height * 0.15,
                                                                         self.view.frame.size.width * 0.8,
                                                                         self.view.frame.size.height * 0.3)];
                             
    [self.reportTextView setDelegate:self];
    [self.reportTextView setReturnKeyType:UIReturnKeyDone];
    self.reportTextView.layer.borderColor = [UIColor blackColor].CGColor;
    self.reportTextView.layer.borderWidth = 1.0f;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.view addSubview:self.reportTextView];
    
    self.timePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(self.view.frame.size.width * 0.1,
                                                                        self.view.frame.size.height * 0.55,
                                                                        self.view.frame.size.width * 0.8,
                                                                        self.view.frame.size.height * 0.3)];
    [self.timePickerView setDataSource: self];
    [self.timePickerView setDelegate: self];
    self.timePickerView.showsSelectionIndicator = YES;
    
    [self.view addSubview:self.timePickerView];
    
    
    UIBarButtonItem* rightB = [[UIBarButtonItem alloc] initWithTitle:@"Send"
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:@selector(onSendPressed:)];
    
    UIBarButtonItem* rightB2 = [[UIBarButtonItem alloc] initWithTitle:@"Send on server"
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(onSendOnServerPressed:)];
    
    
    self.navigationItem.rightBarButtonItems = @[rightB, rightB2];
    
    self.timesArray = @[@"10", kDefaultTime, @"60"];
    self.timeValue = [kDefaultTime intValue];
    [self.timePickerView selectRow:[self.timesArray indexOfObject:kDefaultTime] inComponent:0 animated:NO];
    
    [self reloadMessagesByTime:self.timeValue];
    
    UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(onBackPressed:)];
    self.navigationItem.leftBarButtonItem = backButton;
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)onBackPressed:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{}];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.timesArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString* str = [NSString stringWithFormat:@"%@ min", self.timesArray[row]];
    return str;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.timeValue = [[self.timesArray objectAtIndex:[self.timePickerView selectedRowInComponent:0]] integerValue];
    [self reloadMessagesByTime:self.timeValue];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (IBAction)onSendPressed:(id)sender
{
    NSString* apiKey = [TILog apiKey];
    NSString* instanceId = [TILog instanceId];
    NSString* deviceID = [UIDevice currentDevice].identifierForVendor.UUIDString;
    NSString* timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
    NSString* appStateString = [NSString stringWithFormat:@"\"api_key\" : %@,\n \"instance_id\" : %@,\n \"device_id\" : %@,\n \"unix timestamp\" : %@\n", apiKey, instanceId, deviceID, timestamp];
    
    json11::Json root = jcpp::JHelper::vectorToJson(self.cppMessages);
    NSString* jsonString = [NSString stringWithFormat:@"%s", root.dump().c_str()];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"}," withString:@"},\n"];
    
    NSString* textViewString = [self.reportTextView text];
    
    NSString* stringToSend = [NSString stringWithFormat:@"%@\n Report text: %@\n\n All errors in last %ld mins \n\n %@", appStateString, textViewString, (long)self.timeValue, jsonString];
    
    [self sendMailWithMessageIfPossible:stringToSend];
}

- (void)onSendOnServerPressed:(id)sender
{
    json11::Json root = jcpp::JHelper::vectorToJson(self.cppMessages);
    NSString* jsonString = [NSString stringWithFormat:@"%s", root.dump().c_str()];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"}," withString:@"},\n"];
    
    PFObject *statistics = [PFObject objectWithClassName:@"Statistics"];
    statistics[@"createdTime"] = [NSDate date];
    statistics[@"device_id"] = [[UIDevice currentDevice].identifierForVendor UUIDString];
    statistics[@"applicationInstanceId"] = [TILog instanceId];
    statistics[@"applicationKey"] = [TILog apiKey];
    statistics[@"errorLogs"] = jsonString;
    
    [statistics saveInBackground];
}

- (void)getMessages: (TIOBJLogLevel)level Domain:(NSString*)aDomain tags:(const NSArray*) aTags limit:(NSInteger)limit timeInterval:(NSTimeInterval)time onComplete:(void(^)()) onComplete
{
    std::string domain = aDomain ? [aDomain UTF8String] : "";
    
    auto filter = std::make_shared <ti::Filter>(static_cast<ti::LogLevel>(level), domain, std::set<int>(), limit, time);
    
    auto task = std::make_shared <ti::SQLReadTask>([self, onComplete](ti::SQLResult result){
        NSMutableArray* array = [[NSMutableArray alloc] init];
        auto messages = result.getMessages();
        self.cppMessages = messages;
        for (auto m : messages)
        {
            TIOBJMessage* mes = [TILog toObjCMessage: &m];
            [array addObject:mes];
        }
        self.messages = [array copy];
        onComplete();
    }, filter);
    
    
    ti::DBManager::instance()->pushTask(task);
}

- (void)reloadMessagesByTime:(NSTimeInterval)time
{
    void (^onComplete)() = ^()
    {
        
    };
    [self getMessages:TIOBJLogLevel::Any Domain:@""  tags:nil limit:0 timeInterval:time * 60 onComplete:onComplete];
}

- (void)sendMailWithMessageIfPossible:(NSString*)message
{
    if (![MFMailComposeViewController canSendMail])
    {
        UIAlertController* actionSheet =   [UIAlertController
                                            alertControllerWithTitle:@"Can't send mail"
                                            message:@"You need to enable at least one mail account on the device"
                                            preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction* action  = [UIAlertAction
                                  actionWithTitle:@"Ok"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [actionSheet dismissViewControllerAnimated:YES completion:nil];
                                  }];
        [actionSheet addAction: action];
        [self presentViewController:actionSheet animated:YES completion:nil];
        
    }
    else
    {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"Log Dump"];
        NSString* nonNilMessage = message == nil ? @"" : message;
        [controller setMessageBody:nonNilMessage isHTML:NO];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(nullable NSError *)error
{
    NSLog(@"RESULT %d Error %@", result, error);
    
    [controller dismissViewControllerAnimated:YES completion:^{
    }];
}


@end
