// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#import <Foundation/Foundation.h>

#import <ti-logger/platform/ios/TIOBJMessage.h>

@implementation TIOBJMessage

+(instancetype)init:(enum TIOBJLogLevel)level Code:(int)code Domain:(NSString*)domain Timestamp:(long long)timestamp Description:(NSString*)description
{
	TIOBJMessage* inst = [[TIOBJMessage alloc] init];
	inst->_Level = level;
	inst->_Code = code;
	inst->_Domain = domain;
	inst->_Timestamp = timestamp;
	inst->_Description = description;
	return inst;
}

+(NSString*)levelToString:(enum TIOBJLogLevel)level
{

	switch (level)
	{
		case Error:
			return @"Error";
		case Warning:
			return @"Warning";
		case Info:
			return @"Info";
		case Debug:
			return @"Debug";
		default:
			return @"Any";
	}
	return @"";
}


-(NSString*)LevelStr
{
	return [TIOBJMessage levelToString: self.Level];
}
@end