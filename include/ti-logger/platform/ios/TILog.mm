// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#import <ti-logger/platform/ios/TILog.h>
#include <ti-logger/core/DBManager.h>
#include <ti-logger/core/SQLReadTask.h>
#include <ti-logger/core/SQLDomainsReadTask.h>
#include <ti-logger/core/Log.h>
#include <ti-logger/core/PrintListener.h>
#include <ti-logger/core/LocalListener.h>
#include <ti-logger/core/LANListener.h>
#include <ti-logger/core/WEBListener.h>
#include <ti-logger/platform/ios/TIConstants.h>

@implementation TILog

NSString* getWritablePath()
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString* ret =[NSString stringWithFormat:@"%@/%@/", documentsDirectory, @"database"];
	
	NSError * error = nil;
	[[NSFileManager defaultManager] createDirectoryAtPath:ret
							  withIntermediateDirectories:YES
											   attributes:nil
													error:&error];
	if (error != nil) {
		NSLog(@"Download manager::Error creating directory: %@", error);
		return @"";
	}
	return ret;
}


+ (void)logInitIOS:(NSString*)apiKey instanceId:(NSString*)instanceId
{
	NSString* path = getWritablePath();
	ti::Log::init([path UTF8String], [apiKey UTF8String], [instanceId UTF8String]);
}

+ (NSString*)apiKey
{
    std::string str = ti::Log::instance()->getApiKey();
    return [NSString stringWithCString:str.c_str() encoding:[NSString defaultCStringEncoding]];
}

+ (NSString*)instanceId
{
    std::string str = ti::Log::instance()->getInstanceId();
    return [NSString stringWithCString:str.c_str() encoding:[NSString defaultCStringEncoding]];
}

std::set<int> convertArray(const NSArray* array)
{
	
	std::set<int> ret;
	if (array)
	{
		for (id object in array) {
			ret.insert((int)[object integerValue]);
		}
	}
	
	return ret;
}

+(TIOBJMessage*) toObjCMessage: (const void*)aCPPMessage
{
	const ti::Message* message = static_cast<const ti::Message*>(aCPPMessage);
	
	TIOBJLogLevel level = static_cast<TIOBJLogLevel>(message->Level);
	NSString* domain = [NSString stringWithUTF8String: message->Domain.c_str()];
	if (!message->Domain.c_str())
	{
		message->Domain.c_str();
	}
	
	
	NSString* description = [NSString stringWithUTF8String: message->Description.c_str()];
	
	TIOBJMessage* ret = [TIOBJMessage init:level Code:message->Code Domain:domain Timestamp:message->Timestamp Description:description];
	
	return ret;
}

+(void)addListener:(int)aID
{
    if (TIListenerID::TIPrintListener == aID)
    {
        ti::Log::instance()->addListener(ti::PrintListener::instance());
    }
    else if (TIListenerID::TILocalListener == aID)
    {
        ti::Log::instance()->addListener(ti::LocalListener::instance());
    }
    else if (TIListenerID::TILANListener == aID)
    {
        ti::Log::instance()->addListener(ti::LANListener::instance());
    }
    else if (TIListenerID::TIWEBListener == aID)
    {
        ti::Log::instance()->addListener(ti::WEBListener::instance());
    }
}

+(void)delListener:(int)aID
{
    if (TIListenerID::TIPrintListener == aID)
    {
        ti::Log::instance()->removeListener(ti::PrintListener::instance());
    }
    else if (TIListenerID::TILocalListener == aID)
    {
        ti::Log::instance()->removeListener(ti::LocalListener::instance());
    }
    else if (TIListenerID::TILANListener == aID)
    {
        ti::Log::instance()->removeListener(ti::LANListener::instance());
    }
    else if (TIListenerID::TIWEBListener == aID)
    {
        ti::Log::instance()->removeListener(ti::WEBListener::instance());
    }
}

+(void)configureWEBListener:(NSString* const )aHost port:(NSInteger)aPort
{
    ti::WEBListener::instance()->init([aHost UTF8String], (int)aPort);
}
+(void)configureLANListener:(NSString* const )aHost port:(NSInteger)aPort
{
    ti::LANListener::instance()->configure([aHost UTF8String], (int)aPort);
}

+(void)info:(NSString* const )aDomain tags:(const NSArray*) aTags text:(NSString* const)aText
{
	if (!aDomain || !aText)
	{
		return;
	}
	ti::Log::info(0, [aDomain UTF8String], convertArray(aTags), [aText UTF8String]);
}

+(void)warning:(NSString* const )aDomain tags:(const NSArray*) aTags text:(NSString* const)aText
{
	if (!aDomain || !aText)
	{
		return;
	}
	ti::Log::warning(0, [aDomain UTF8String], convertArray(aTags), [aText UTF8String]);
}

+(void)error:(int) aCode domain:(NSString* const )aDomain tags:(const NSArray*) aTags text:(NSString* const)aText
{
	if (!aDomain || !aText)
	{
		return;
	}
	ti::Log::error(aCode, [aDomain UTF8String], convertArray(aTags), [aText UTF8String]);
}

+(void)getDomains:(void(^)(NSArray* messages)) onComplete
{
	
	NSMutableArray* array = [[NSMutableArray alloc] init];
	auto task = std::make_shared <ti::SQLDomainsReadTask>([array, onComplete](ti::SQLResult result){
		auto data = result.data;
		for (auto m : data)
		{
			if (m.size())
			{
				NSString* s = [NSString stringWithUTF8String:  m[0].c_str()];
				[array addObject:s];
			}
		}
		onComplete(array);
	});
	
	ti::DBManager::instance()->pushTask(task);
	
}

+(void)getMessages: (TIOBJLogLevel)level Domain:(NSString*)aDomain tags:(const NSArray*) aTags limit:(NSInteger)limit onComplete:(void(^)(NSArray* messages)) onComplete
{
	NSMutableArray* array = [[NSMutableArray alloc] init];
	
	std::string domain = aDomain ? [aDomain UTF8String] : "";
	
	auto filter = std::make_shared <ti::Filter>(static_cast<ti::LogLevel>(level), domain, convertArray(aTags), limit);
	
	auto task = std::make_shared <ti::SQLReadTask>([array, onComplete](ti::SQLResult result){
		auto messages = result.getMessages();
		for (auto m : messages)
		{
			TIOBJMessage* mes = [TILog toObjCMessage :&m];
			[array addObject:mes];
		}
		onComplete(array);
	}, filter);
	
	
	ti::DBManager::instance()->pushTask(task);
}

+(void)dropContentTable
{
	ti::DBManager::instance()->drop();
}

@end